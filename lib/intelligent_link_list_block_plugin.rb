class IntelligentLinkListBlockPlugin < Noosfero::Plugin

  def self.plugin_name
    "Intelligent Link List"
  end

  def self.plugin_description
    _("A plugin for automatic multiplis sublinks.")
  end

  def self.extra_blocks
    {
      IntelligentLinkListBlockPlugin::IntelligentLinkListBlock => {}
    }
  end

  def self.has_admin_url?
    false
  end

  def stylesheet?
    true
  end

end
