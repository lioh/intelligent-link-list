class IntelligentLinkListBlock < Block


  def self.pretty_name
    _('Intelligent Link list')
  end

  def self.description
    _("A plugin for automatic multiplis sublinks.")
  end

  def help
    _("")
  end

  def cacheable?
    false
  end

  def layout_template
    nil
  end

end
