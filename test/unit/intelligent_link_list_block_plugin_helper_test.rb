require 'test_helper'
class IntelligentLinkListBlockPluginHelperTest < ActiveSupport::TestCase
  _dir = File.dirname(__FILE__)
  require File.join _dir, '../../helpers/intelligent_link_list_block_plugin_helper'
  include IntelligentLinkListBlockPluginHelper
	helper = IntelligentLinkListBlockPluginHelper

  def url_simple url
    "#{url[:controller]}::#{url[:action]} " +
    url.map do |k,v|
      unless [:controller, :action].include? k
        "#{k}:#{k==:page ? v.join('/') : v}"
      end
    end.compact.join(' ')
  end

  def setup
      @person = fast_create(Person, name:"Tanner", identifier:"tanner")
      pid = @person.id
      @grandfather = fast_create(Folder, name:"grandfather", profile_id:pid)
      @father = fast_create(Folder, name:"father", profile_id:pid,
        parent_id:@grandfather.id)
      @son = fast_create(Folder, name:"son", profile_id:pid,
        parent_id:@father.id)
      @grandson = fast_create(TextArticle, name:"grandson", profile_id:pid,
        parent_id:@son.id)

      @blog = fast_create(Blog, name:"blog", profile_id:pid, slug:"blog")
      @blog.save!
      @post1 = fast_create(TextArticle, name:"post1", profile_id:pid,
        parent_id:@blog.id, published_at:"2018-01-01")
      @post2 = fast_create(TextArticle, name:"post2", profile_id:pid,
        parent_id:@blog.id, published_at:"2018-01-02")
      @post3 = fast_create(TextArticle, name:"post3", profile_id:pid,
        parent_id:@blog.id, published_at:"2018-02-01")
  end

  should "get folder ancestors" do
    assert_equal ["Tanner", "grandfather"],
      helper.get_ancestors(@grandfather).map(&:name)
    assert_equal ["Tanner", "grandfather", "father"],
      helper.get_ancestors(@father).map(&:name)
    assert_equal ["Tanner", "grandfather", "father", "son"],
      helper.get_ancestors(@son).map(&:name)
    assert_equal ["Tanner", "grandfather", "father", "son"],
      helper.get_ancestors(@grandson).map(&:name)
  end

  should "get profile on get ancestors of profile" do
    assert_equal ["Tanner"], helper.get_ancestors(@person).map(&:name)
  end

  should "get children of a profile" do
    assert_equal ["blog", "grandfather"], helper.get_children(@person).map { |c| c[:label] }.sort
  end

  should "get children of a folder" do
    assert_equal ["son"], helper.get_children(@father).map { |c| c[:label] }
  end

  should "get children from blog must get a month list" do
    assert_equal [
      {
        label:"Jan 2018",
        href:"content_viewer::view_page host:localhost profile:tanner page:blog year:2018 month:1",
        object:@blog
      },
      {
        label:"Feb 2018",
        href:"content_viewer::view_page host:localhost profile:tanner page:blog year:2018 month:2",
        object:@blog
      }
    ], helper.get_children(@blog).map{ |c| c[:href] = url_simple c[:href]; c }
  end
  
  should "get children from gallery must get sub gallery list" do
		pid = @person.id
   	g0 = fast_create(Gallery, name:"Gallery 0", profile_id:pid, slug:"gallery-0")
   	g0.save!
   	fast_create(TextArticle, name:"Post 1", profile_id:pid, parent_id:g0.id)
   	g1 = fast_create(Gallery, name:"Gallery 1", profile_id:pid, parent_id:g0.id)
   	g1.save!
   	g2 = fast_create(Gallery, name:"Gallery 2", profile_id:pid, parent_id:g0.id)
   	g2.save!
    assert_equal [
      {
        label:"Gallery 1",
        href:"content_viewer::view_page host:localhost profile:tanner page:gallery-0/gallery-1",
        object:g1
      },
      {
        label:"Gallery 2",
        href:"content_viewer::view_page host:localhost profile:tanner page:gallery-0/gallery-2",
        object:g2
      }
    ], helper.get_children(g0).map{ |c| c[:href] = url_simple c[:href]; c }
  end

  should "get children from folder must get contents list" do
		pid = @person.id
   	f0 = fast_create(Folder, name:"Parent", profile_id:pid)
   	f0.save!
   	f = fast_create(Folder, name:"Folder", profile_id:pid, parent_id:f0.id)
   	f.save!
   	a = fast_create(TextArticle, name:"Post", profile_id:pid, parent_id:f0.id)
   	a.save!
   	g = fast_create(Gallery, name:"Gallery", profile_id:pid, parent_id:f0.id)
   	g.save!
   	b = fast_create(Blog, name:"Blog", profile_id:pid, parent_id:f0.id)
   	b.save!
    assert_equal [
    	{
        label:"Blog",
        href:"content_viewer::view_page host:localhost profile:tanner page:parent/blog",
        object:b
      },
      {
        label:"Folder",
        href:"content_viewer::view_page host:localhost profile:tanner page:parent/folder",
        object:f
      },
      {
        label:"Gallery",
        href:"content_viewer::view_page host:localhost profile:tanner page:parent/gallery",
        object:g
      }
    ], helper.get_children(f0).map{ |c| c[:href] = url_simple c[:href]; c }
  end

end
