_dir = File.dirname(__FILE__)
require File.join _dir + '../../../helpers/intelligent_link_list_block_plugin_helper'

class IntelligentLinkListBlockPluginController < ProfileController
  include IntelligentLinkListBlockPluginHelper
end
